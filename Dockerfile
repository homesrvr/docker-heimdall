FROM linuxserver/heimdall:latest

VOLUME /config
EXPOSE 80 443
HEALTHCHECK --interval=1m --timeout=5s --start-period=15s \
    CMD curl -fsSL "http://localhost:80/" || exit 1

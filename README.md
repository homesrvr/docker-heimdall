# docker-heimdall

[![pipeline status](https://gitlab.com/homesrvr/docker-heimdall/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-heimdall/commits/main)
[![Heimdall Release](https://gitlab.com/homesrvr/docker-heimdall/-/jobs/artifacts/main/raw/release.svg?job=publish_badge)](https://gitlab.com/homesrvr/docker-heimdall/-/jobs/artifacts/main/raw/release.txt?job=publish_badge)
[![Docker link](https://gitlab.com/homesrvr/docker-heimdall/-/jobs/artifacts/main/raw/dockerimage.svg?job=publish_badge)](https://hub.docker.com/r/culater/heimdall)

